(function() {
    "use strict";

    //slider

    $(window).load(function() {
        $('.flexslider').flexslider({
            animation: "slide",
            slideshowSpeed: 3000,
            controlNav: false,
            directionNav: false,
        });
    });

    //carousel

    $(".owl-carousel1").owlCarousel({
        loop: true,
        items: 6,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsiveClass: true,
        dots:false,
        responsive: {
            0: {
                items: 2,
            },
            768: {
                items: 4,
            },
            1024: {
                items: 6,
            }
        }
    });

    $(".owl-carousel").owlCarousel({
        loop: true,
        items: 4,
        dots:false,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2,
            },
            1024: {
                items: 4,
            }
        }
    });

    $('.owl-carousel2').owlCarousel({
        loop:true,
        nav:false,
        items:1,
        margin:10,
        dots:true,
        autoplay: true,
    });

    $('.owl-carousel3').owlCarousel({
        loop:true,
        nav:true,
        navText: ["<p><i class='fa fa-angle-left' aria-hidden='true'></i></p>","<p><i class='fa fa-angle-right' aria-hidden='true'></i></p>"],
        items:1,
        autoplay: true,
    });

    $('.owl-carousel4').owlCarousel({
        loop:true,
        nav: true,
        navText: ["<p><i class='fa fa-angle-left' aria-hidden='true'></i></p>","<p><i class='fa fa-angle-right' aria-hidden='true'></i></p>"],
        items:4,
        autoplay: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
            },
            768: {
                items: 3,
            },
            1024: {
                items: 4,
            }
        }
    });


   

    //set same height

    var highestBox = 0;
    var highestBox1;
    var highestBox2;
    var highestBox3;
    $('.services-in img').each(function() {
        if ($(this).height() > highestBox) {
            highestBox = $(this).height();
            highestBox1 = highestBox;
        }
        return highestBox1;
    });

    $('.service-room5 img').each(function() {
        if ($(this).height() > highestBox) {
            highestBox = $(this).height();
            highestBox2 = highestBox;
        }
        return highestBox2;
    });

    $('.some-in img').each(function() {
        if ($(this).height() > highestBox) {
            highestBox = $(this).height();
            highestBox3 = highestBox;
        }
        return highestBox3;
    });

    $('.services-in img').height(highestBox1);
    $('.service-room5 img').height(highestBox2);
    $('.some-in img').height(highestBox3);

    //set same width

    var minwidth = 100;
    var minwidth1;

    $('.services-in img').each(function() {
        if ($(this).width() < minwidth) {
            minwidth = $(this).width();
            minwidth1 = minwidth;
        }
        return minwidth1;
    });

    $('.services-in img').width(minwidth1);

    //counter
    $('.counter').counterUp({
        delay: 10,
        time: 4000
    });

    //lightbox

    $(".youtube").colorbox({
        iframe: true,
        innerWidth: "80%",
        innerHeight: "80%"
    });

    //date pick
    $("#datepicker1").datepicker();
    $("#datepicker2").datepicker();

    //menu

    $.fn.menumaker = function(options) {

        var cssmenu = $(this),
            settings = $.extend({
                title: "Menu",
                format: "dropdown",
                sticky: false
            }, options);

        return this.each(function() {
            cssmenu.prepend('<div id="menu-button">' + settings.title + '</div>');
            $(this).find("#menu-button").on('click', function() {
                $(this).toggleClass('menu-opened');
                var mainmenu = $(this).next('ul');
                if (mainmenu.hasClass('open')) {
                    mainmenu.hide().removeClass('open');
                } else {
                    mainmenu.show().addClass('open');
                    if (settings.format === "dropdown") {
                        mainmenu.find('ul').show();
                    }
                }
            });

            cssmenu.find('li ul').parent().addClass('has-sub');

            var multiTg = function() {
                cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
                cssmenu.find('.submenu-button').on('click', function() {
                    $(this).toggleClass('submenu-opened');
                    if ($(this).siblings('ul').hasClass('open')) {
                        $(this).siblings('ul').removeClass('open').hide();
                    } else {
                        $(this).siblings('ul').addClass('open').show();
                    }
                });
            };

            if (settings.format === 'multitoggle') multiTg();
            else cssmenu.addClass('dropdown');

            if (settings.sticky === true) cssmenu.css('position', 'fixed');

            var resizeFix = function() {
                if ($(window).width() > 768) {
                    cssmenu.find('ul').show();
                }

                if ($(window).width() <= 768) {
                    cssmenu.find('ul').hide().removeClass('open');
                }
            };
            resizeFix();
            return $(window).on('resize', resizeFix);

        });
    };

    //search
    $('.search p').click(function() {
        var a = $(this).parent().find('.form-search').toggleClass('close-search');
    })

    //select effect

    $(document).ready(function() {
        $("#cssmenu").menumaker({
            format: "multitoggle"
        });

    });

})(jQuery);